﻿namespace MainForm
{
    partial class Movies
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtDirector = new System.Windows.Forms.TextBox();
            this.txtActors = new System.Windows.Forms.TextBox();
            this.comboGenre = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.groupBoxMovies = new System.Windows.Forms.GroupBox();
            this.checkBoxAvailable = new System.Windows.Forms.CheckBox();
            this.checkBoxPremiere = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.groupBoxMovies.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Movie Genre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Movie Director";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Movie Title";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Movie Actors";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(92, 29);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(314, 20);
            this.txtTitle.TabIndex = 43;
            // 
            // txtDirector
            // 
            this.txtDirector.Location = new System.Drawing.Point(91, 63);
            this.txtDirector.Name = "txtDirector";
            this.txtDirector.Size = new System.Drawing.Size(191, 20);
            this.txtDirector.TabIndex = 44;
            // 
            // txtActors
            // 
            this.txtActors.Location = new System.Drawing.Point(91, 189);
            this.txtActors.Name = "txtActors";
            this.txtActors.Size = new System.Drawing.Size(384, 20);
            this.txtActors.TabIndex = 46;
            // 
            // comboGenre
            // 
            this.comboGenre.FormattingEnabled = true;
            this.comboGenre.Items.AddRange(new object[] {
            "Romance",
            "Mystery",
            "Comedy",
            "Science Fiction",
            "Drama",
            "Musical",
            "Crime"});
            this.comboGenre.Location = new System.Drawing.Point(92, 102);
            this.comboGenre.Name = "comboGenre";
            this.comboGenre.Size = new System.Drawing.Size(121, 21);
            this.comboGenre.TabIndex = 47;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnClose);
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Controls.Add(this.btnInsert);
            this.groupBox2.Location = new System.Drawing.Point(88, 260);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(350, 206);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actions";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Snow;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(98, 155);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(155, 35);
            this.btnClose.TabIndex = 72;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnUpdate.Font = new System.Drawing.Font("Modern No. 20", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(98, 97);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(155, 39);
            this.btnUpdate.TabIndex = 70;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.BackColor = System.Drawing.Color.LightSalmon;
            this.btnInsert.Font = new System.Drawing.Font("Modern No. 20", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.Location = new System.Drawing.Point(93, 38);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(155, 39);
            this.btnInsert.TabIndex = 69;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = false;
            this.btnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // groupBoxMovies
            // 
            this.groupBoxMovies.Controls.Add(this.checkBoxAvailable);
            this.groupBoxMovies.Controls.Add(this.checkBoxPremiere);
            this.groupBoxMovies.Controls.Add(this.label2);
            this.groupBoxMovies.Controls.Add(this.txtActors);
            this.groupBoxMovies.Controls.Add(this.label3);
            this.groupBoxMovies.Controls.Add(this.comboGenre);
            this.groupBoxMovies.Controls.Add(this.label4);
            this.groupBoxMovies.Controls.Add(this.txtDirector);
            this.groupBoxMovies.Controls.Add(this.txtTitle);
            this.groupBoxMovies.Controls.Add(this.label7);
            this.groupBoxMovies.Location = new System.Drawing.Point(12, 12);
            this.groupBoxMovies.Name = "groupBoxMovies";
            this.groupBoxMovies.Size = new System.Drawing.Size(508, 229);
            this.groupBoxMovies.TabIndex = 70;
            this.groupBoxMovies.TabStop = false;
            this.groupBoxMovies.Text = "Movie Summary";
            // 
            // checkBoxAvailable
            // 
            this.checkBoxAvailable.AutoSize = true;
            this.checkBoxAvailable.Location = new System.Drawing.Point(93, 163);
            this.checkBoxAvailable.Name = "checkBoxAvailable";
            this.checkBoxAvailable.Size = new System.Drawing.Size(69, 17);
            this.checkBoxAvailable.TabIndex = 49;
            this.checkBoxAvailable.Text = "Available";
            this.checkBoxAvailable.UseVisualStyleBackColor = true;
            // 
            // checkBoxPremiere
            // 
            this.checkBoxPremiere.AutoSize = true;
            this.checkBoxPremiere.Location = new System.Drawing.Point(93, 140);
            this.checkBoxPremiere.Name = "checkBoxPremiere";
            this.checkBoxPremiere.Size = new System.Drawing.Size(67, 17);
            this.checkBoxPremiere.TabIndex = 48;
            this.checkBoxPremiere.Text = "Premiere";
            this.checkBoxPremiere.UseVisualStyleBackColor = true;
            // 
            // Movies
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(555, 483);
            this.Controls.Add(this.groupBoxMovies);
            this.Controls.Add(this.groupBox2);
            this.Name = "Movies";
            this.Text = "Movies";
            this.groupBox2.ResumeLayout(false);
            this.groupBoxMovies.ResumeLayout(false);
            this.groupBoxMovies.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtDirector;
        private System.Windows.Forms.TextBox txtActors;
        private System.Windows.Forms.ComboBox comboGenre;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.GroupBox groupBoxMovies;
        private System.Windows.Forms.CheckBox checkBoxAvailable;
        private System.Windows.Forms.CheckBox checkBoxPremiere;
    }
}