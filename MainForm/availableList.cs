﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class Reports_available_movies_ : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int selectedAvailableID;

        public Reports_available_movies_()
        {
            InitializeComponent();
        }


        private void Reports_available_movies_Load(object sender, EventArgs e)
        {
        }

        private void LoadAvailableMovies()
        {
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM movies  WHERE movie_availability LIKE 'Yes'";
                comm.ExecuteNonQuery();
                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                adapter.Fill(table);
                dgvMovies.DataSource = table;
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
        }
        private void Reports_available_movies__Load(object sender, EventArgs e)
        {
            LoadAvailableMovies();
        }

        private void BtnDelete_Click_1(object sender, EventArgs e)
        {
            DialogResult questionDelete = MessageBox.Show($"Are you sure you want to delete this movie, ID={selectedAvailableID}?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (questionDelete == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"DELETE FROM movies WHERE movie_id = {selectedAvailableID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    LoadAvailableMovies();
                    MessageBox.Show("Record successfully deleted!");

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }

        private void DgvMovies_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            selectedAvailableID = Convert.ToInt16(dgvMovies.Rows[e.RowIndex].Cells[0].Value.ToString());
        }
    }
}
