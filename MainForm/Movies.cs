﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;


namespace MainForm
{
    public partial class Movies : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int movieID;

        public Movies()
        {
            InitializeComponent();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            if (txtTitle.Text != "" && txtDirector.Text != "" && comboGenre.Text != "" && txtActors.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        if (checkBoxPremiere.Checked == false && checkBoxAvailable.Checked == false)
                        {
                            comm.CommandText = String.Format($"INSERT INTO movies VALUES ('{txtTitle.Text}', '{txtDirector.Text}', '{comboGenre.Text}', 'No', 'No', '{txtActors.Text}')");

                        }
                        else if (checkBoxPremiere.Checked == true && checkBoxAvailable.Checked == false)
                        {
                            comm.CommandText = String.Format($"INSERT INTO movies VALUES ('{txtTitle.Text}', '{txtDirector.Text}', '{comboGenre.Text}', 'Yes', 'No', '{txtActors.Text}')");

                        }
                        else if (checkBoxPremiere.Checked == false && checkBoxAvailable.Checked == true)
                        {
                            comm.CommandText = String.Format($"INSERT INTO movies VALUES ('{txtTitle.Text}', '{txtDirector.Text}', '{comboGenre.Text}', 'No', 'Yes', '{txtActors.Text}')");
                        }
                        else
                        {
                            comm.CommandText = String.Format($"INSERT INTO movies VALUES ('{txtTitle.Text}', '{txtDirector.Text}', '{comboGenre.Text}', 'Yes', 'Yes', '{txtActors.Text}')");
                        }
                        comm.ExecuteNonQuery();
                        connection.Close();
                        btnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Record successfully inserted!");
                        txtTitle.Text = "";
                        txtDirector.Text = "";
                        comboGenre.Text = "";
                        checkBoxPremiere.Checked = false;
                        checkBoxAvailable.Checked = false;
                        txtActors.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                }
            }
        }

        private int GetMovieID()
        {
            int movieID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT movie_id FROM movies WHERE movie_title = '{txtTitle.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                movieID = Convert.ToInt16(table.Rows[0]["movie_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return movieID;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult question = MessageBox.Show($"Are you sure you want to update the movie: {txtTitle.Text}?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (question == DialogResult.Yes)
            {
                try
                {
                    movieID = GetMovieID();
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    if (checkBoxPremiere.Checked == false && checkBoxAvailable.Checked == false)
                    {
                        comm.CommandText = String.Format($"UPDATE movies SET movie_title = '{txtTitle.Text}', movie_director = '{txtDirector.Text}', movie_genre = '{comboGenre.Text}', movie_premiere = 'No', movie_availability = 'No', movie_actors = '{txtActors.Text}'  WHERE movie_id = {movieID}");
                    }
                    else if (checkBoxPremiere.Checked == true && checkBoxAvailable.Checked == false)
                    {
                        comm.CommandText = String.Format($"UPDATE movies SET movie_title = '{txtTitle.Text}', movie_director = '{txtDirector.Text}', movie_genre = '{comboGenre.Text}', movie_premiere = 'Yes', movie_availability = 'No', movie_actors = '{txtActors.Text}'  WHERE movie_id = {movieID}");
                    }
                    else if (checkBoxPremiere.Checked == false && checkBoxAvailable.Checked == true)
                    {
                        comm.CommandText = String.Format($"UPDATE movies SET movie_title = '{txtTitle.Text}', movie_director = '{txtDirector.Text}', movie_genre = '{comboGenre.Text}', movie_premiere = 'No', movie_availability = 'Yes', movie_actors = '{txtActors.Text}'  WHERE movie_id = {movieID}");
                    }
                    else
                    {
                        comm.CommandText = String.Format($"UPDATE movies SET movie_title = '{txtTitle.Text}', movie_director = '{txtDirector.Text}', movie_genre = '{comboGenre.Text}', movie_premiere = 'Yes', movie_availability = 'Yes', movie_actors = '{txtActors.Text}'  WHERE movie_id = {movieID}");
                    }

                    comm.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Record successfully updated!");
                    txtTitle.Text = "";
                    txtDirector.Text = "";
                    comboGenre.Text = "";
                    checkBoxPremiere.Checked = false;
                    checkBoxAvailable.Checked = false;
                    txtActors.Text = "";
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }

       

    }
}
