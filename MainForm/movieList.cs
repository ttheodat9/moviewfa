﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class Reports_movie_list_ : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int selectedMovieID;

        public Reports_movie_list_()
        {
            InitializeComponent();
        }
        private void LoadAllMovies()
        {
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM movies";
                comm.ExecuteNonQuery();
                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                adapter.Fill(table);
                dgvMovies.DataSource = table;
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
        }


        private void Reports_movie_list__Load(object sender, EventArgs e)
        {
            LoadAllMovies();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult questionDelete = MessageBox.Show($"Are you sure you want to delete this movie, ID={selectedMovieID}?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (questionDelete == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"DELETE FROM movies WHERE movie_id = {selectedMovieID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    LoadAllMovies();
                    MessageBox.Show("Record successfully deleted!");

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }
        private void dgvMovies_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            selectedMovieID = Convert.ToInt16(dgvMovies.Rows[e.RowIndex].Cells[0].Value.ToString());
        }
    }
}
