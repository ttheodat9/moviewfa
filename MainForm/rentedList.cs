﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class Reports_rented_movies_ : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int selectedRentedID;

        public Reports_rented_movies_()
        {
            InitializeComponent();
        }

        private void LoadRents()
        {
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT movies.movie_id, movies.movie_title, movies.movie_director, rents.user_id, rents.rent_date, rents.rent_return_date, rents.rent_price FROM movies INNER JOIN rents ON movies.movie_id = rents.movie_id";
                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                adapter.Fill(table);
                dgvMovies.DataSource = table;
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
        }

        private void Reports_rented_movies__Load(object sender, EventArgs e)
        {
            LoadRents();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult questionDelete = MessageBox.Show($"Are you sure you want to delete this rent, ID={selectedRentedID}?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (questionDelete == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"DELETE FROM rents WHERE rent_id = {selectedRentedID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    LoadRents();
                    MessageBox.Show("Record successfully deleted!");

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }

        private void dgvRent_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            selectedRentedID = Convert.ToInt16(dgvMovies.Rows[e.RowIndex].Cells[0].Value.ToString());
        }
    }
}
