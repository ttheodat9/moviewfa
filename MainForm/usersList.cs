﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class Reports_user_list_ : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int selectedUserID;
        public Reports_user_list_()
        {
            InitializeComponent();
        }
        private void LoadUsers()
        {
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM users";
                comm.ExecuteNonQuery();
                DataTable table = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                adapter.Fill(table);
                dgvUsers2.DataSource = table;
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
        }
        
        private void Reports_user_list__Load_1(object sender, EventArgs e)
        {
            LoadUsers();
        }

        private void BtnDelete_Click_1(object sender, EventArgs e)
        {
            DialogResult questionDelete = MessageBox.Show($"Are you sure you want to delete this user, ID: {selectedUserID}?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (questionDelete == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"DELETE FROM users WHERE user_id = {selectedUserID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    LoadUsers();
                    MessageBox.Show("Record successfully deleted!");

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }

        private void DgvUsers_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            selectedUserID = Convert.ToInt16(dgvUsers2.Rows[e.RowIndex].Cells[0].Value.ToString());
        }
    }
}
