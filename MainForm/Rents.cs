﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;


namespace MainForm
{
    public partial class Rents : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int rentID;
        string title;
        string fullName;

        public Rents()
        {
            InitializeComponent();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            if (dateRent.Text != "" && dateReturn.Text != "" && txtPrice.Text != "" && txtMovieID.Text != "" && txtUserID.Text != "")
            {
                DialogResult question = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        DateTime rental = dateRent.Value;
                        DateTime returnDate = dateReturn.Value;
                        connection.Open();
                         SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO rents(rent_date, rent_return_date, rent_price, movie_id, user_id) VALUES ('{rental}', '{returnDate}', '{txtPrice.Text}', {txtMovieID.Text}, {txtUserID.Text})");
                        connection.Close();
                        btnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Record successfully inserted!");
                        txtPrice.Text = "";
                        txtMovieID.Text = "";
                        txtUserID.Text = "";
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                }
            }
        }
        private int GetRentID()
        {
            int rentID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT rent_id FROM rents WHERE movie_id LIKE '{txtMovieID.Text}' AND user_id LIKE '{txtUserID.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                rentID = Convert.ToInt16(table.Rows[0]["rent_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return rentID;
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            rentID = GetRentID();
            DialogResult question = MessageBox.Show($"Are you sure you want to update the rent made on {dateRent.Value} with the ID:{rentID}?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (question == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"UPDATE rents SET rent_date = '{dateRent.Value}', rent_return_date = '{dateReturn.Value}', rent_price = {txtPrice.Text}, movie_id = {txtMovieID.Text}, user_id = {txtUserID.Text}  WHERE rent_id = {rentID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    MessageBox.Show("Record successfully updated!");
                    dateRent.Text = "";
                    dateReturn.Text = "";
                    txtPrice.Text = "";
                    txtMovieID.Text = "";
                    txtUserID.Text = "";

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }
        private int GetMovieID()
        {
            int movieID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT movie_id FROM movies WHERE movie_title LIKE '{title}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                movieID = Convert.ToInt16(table.Rows[0]["movie_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return movieID;
        }
        private void MovieID_onclick(object sender, EventArgs e)
        {
            int MovieID;
            string question = Interaction.InputBox($"Type the title of the movie you are renting:", "movie title");
            title = question;
            MovieID = GetMovieID();
            txtMovieID.Text = Convert.ToString(MovieID);
        }

        private int GetUserID()
        {
            int userID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT user_id FROM users WHERE user_name LIKE '{fullName}' ");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                userID = Convert.ToInt16(table.Rows[0]["user_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return userID;
        }
        private void UserID_onclick(object sender, EventArgs e)
        {
            int UserID;
            string questionName = Interaction.InputBox($"Type your name to find your id:", "full name");
            fullName = questionName;
            UserID = GetUserID();
            txtUserID.Text = Convert.ToString(UserID);
        }

        


    }
}
