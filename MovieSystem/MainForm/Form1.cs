﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void tsmRent_Click(object sender, EventArgs e)
        {
            Rents rents = new Rents();
            rents.Show();
        }

        private void tsmMovies_Click(object sender, EventArgs e)
        {
            Movies movies = new Movies();
            movies.Show();
        }

        private void tsmUsers_Click(object sender, EventArgs e)
        {
            Users users = new Users();
            users.Show();
        }


        private void ListOfUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports_user_list_ user_List_ = new Reports_user_list_();
            user_List_.Show();
        }

        private void ListOfMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports_movie_list_ movie_List_ = new Reports_movie_list_();
            movie_List_.Show();
        }

        private void AllAvailableMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports_available_movies_ available_movies_= new Reports_available_movies_();
            available_movies_.Show();
        }

        private void AllRentedMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports_rented_movies_ rented_movies_ = new Reports_rented_movies_();
            rented_movies_.Show();
        }

        private void TitleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchTitle title = new searchTitle();
            title.Show();
        }

        private void DirectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchDirector director = new searchDirector();
            director.Show();
        }

        private void GenreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchGenre genre = new searchGenre();
            genre.Show();
        }
    }
}
