﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace MainForm
{
    public partial class searchDirector : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");

        public searchDirector()
        {
            InitializeComponent();
        }

        private void ShowAll()
        {
            try
            {
                string queryStr = $"SELECT * FROM movies WHERE movie_director LIKE '{comboSearch.Text}'";
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = queryStr;
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                dgvMovies.DataSource = table;
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
        }
        private DataTable GetMovieInfo()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT * FROM movies";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }

        private void SearchDirector_Load(object sender, EventArgs e)
        {
            labelSearch.Text = "Search a movie by the director:";
            comboSearch.DataSource = GetMovieInfo();
            comboSearch.DisplayMember = "movie_director";
        }

        private void ComboSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowAll();
        }

    
    }
}
