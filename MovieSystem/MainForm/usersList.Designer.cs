﻿namespace MainForm
{
    partial class Reports_user_list_
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reports_user_list_));
            this.dgvUsers2 = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers2)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvUsers2
            // 
            this.dgvUsers2.AllowUserToAddRows = false;
            this.dgvUsers2.BackgroundColor = System.Drawing.Color.Coral;
            this.dgvUsers2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsers2.Location = new System.Drawing.Point(12, 168);
            this.dgvUsers2.Name = "dgvUsers2";
            this.dgvUsers2.RowHeadersVisible = false;
            this.dgvUsers2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsers2.Size = new System.Drawing.Size(562, 139);
            this.dgvUsers2.TabIndex = 1;
            this.dgvUsers2.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvUsers_RowEnter);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Font = new System.Drawing.Font("Modern No. 20", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(209, 352);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(155, 39);
            this.btnDelete.TabIndex = 72;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.BtnDelete_Click_1);
            // 
            // Reports_user_list_
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(577, 450);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dgvUsers2);
            this.Name = "Reports_user_list_";
            this.Text = "Reports_user_list_";
            this.Load += new System.EventHandler(this.Reports_user_list__Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvUsers2;
        private System.Windows.Forms.Button btnDelete;
    }
}