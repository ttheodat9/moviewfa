﻿namespace MainForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tsmMaintenance = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmMovies = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmUsers = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRent = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.titleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.directorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfUsersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listOfMoivesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allAvailableMoviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allRentedMoviesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsmMaintenance
            // 
            this.tsmMaintenance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmMovies,
            this.tsmUsers,
            this.tsmRent});
            this.tsmMaintenance.Name = "tsmMaintenance";
            this.tsmMaintenance.Size = new System.Drawing.Size(88, 20);
            this.tsmMaintenance.Text = "Maintenance";
            // 
            // tsmMovies
            // 
            this.tsmMovies.Name = "tsmMovies";
            this.tsmMovies.Size = new System.Drawing.Size(112, 22);
            this.tsmMovies.Text = "Movies";
            this.tsmMovies.Click += new System.EventHandler(this.tsmMovies_Click);
            // 
            // tsmUsers
            // 
            this.tsmUsers.Name = "tsmUsers";
            this.tsmUsers.Size = new System.Drawing.Size(112, 22);
            this.tsmUsers.Text = "Users";
            this.tsmUsers.Click += new System.EventHandler(this.tsmUsers_Click);
            // 
            // tsmRent
            // 
            this.tsmRent.Name = "tsmRent";
            this.tsmRent.Size = new System.Drawing.Size(112, 22);
            this.tsmRent.Text = "Rents";
            this.tsmRent.Click += new System.EventHandler(this.tsmRent_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.titleToolStripMenuItem,
            this.directorToolStripMenuItem,
            this.genreToolStripMenuItem});
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.usersToolStripMenuItem.Text = "Search by";
            // 
            // titleToolStripMenuItem
            // 
            this.titleToolStripMenuItem.Name = "titleToolStripMenuItem";
            this.titleToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.titleToolStripMenuItem.Text = "Title";
            this.titleToolStripMenuItem.Click += new System.EventHandler(this.TitleToolStripMenuItem_Click);
            // 
            // directorToolStripMenuItem
            // 
            this.directorToolStripMenuItem.Name = "directorToolStripMenuItem";
            this.directorToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.directorToolStripMenuItem.Text = "Director";
            this.directorToolStripMenuItem.Click += new System.EventHandler(this.DirectorToolStripMenuItem_Click);
            // 
            // genreToolStripMenuItem
            // 
            this.genreToolStripMenuItem.Name = "genreToolStripMenuItem";
            this.genreToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.genreToolStripMenuItem.Text = "Genre";
            this.genreToolStripMenuItem.Click += new System.EventHandler(this.GenreToolStripMenuItem_Click);
            // 
            // rentsToolStripMenuItem
            // 
            this.rentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listOfUsersToolStripMenuItem,
            this.listOfMoivesToolStripMenuItem,
            this.allAvailableMoviesToolStripMenuItem,
            this.allRentedMoviesToolStripMenuItem});
            this.rentsToolStripMenuItem.Name = "rentsToolStripMenuItem";
            this.rentsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.rentsToolStripMenuItem.Text = "Reports";
            // 
            // listOfUsersToolStripMenuItem
            // 
            this.listOfUsersToolStripMenuItem.Name = "listOfUsersToolStripMenuItem";
            this.listOfUsersToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listOfUsersToolStripMenuItem.Text = "List of Users";
            this.listOfUsersToolStripMenuItem.Click += new System.EventHandler(this.ListOfUsersToolStripMenuItem_Click);
            // 
            // listOfMoivesToolStripMenuItem
            // 
            this.listOfMoivesToolStripMenuItem.Name = "listOfMoivesToolStripMenuItem";
            this.listOfMoivesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listOfMoivesToolStripMenuItem.Text = "List of Movies";
            this.listOfMoivesToolStripMenuItem.Click += new System.EventHandler(this.ListOfMoviesToolStripMenuItem_Click);
            // 
            // allAvailableMoviesToolStripMenuItem
            // 
            this.allAvailableMoviesToolStripMenuItem.Name = "allAvailableMoviesToolStripMenuItem";
            this.allAvailableMoviesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.allAvailableMoviesToolStripMenuItem.Text = "All Available Movies";
            this.allAvailableMoviesToolStripMenuItem.Click += new System.EventHandler(this.AllAvailableMoviesToolStripMenuItem_Click);
            // 
            // allRentedMoviesToolStripMenuItem
            // 
            this.allRentedMoviesToolStripMenuItem.Name = "allRentedMoviesToolStripMenuItem";
            this.allRentedMoviesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.allRentedMoviesToolStripMenuItem.Text = "All Rented Movies";
            this.allRentedMoviesToolStripMenuItem.Click += new System.EventHandler(this.AllRentedMoviesToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmMaintenance,
            this.usersToolStripMenuItem,
            this.rentsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(713, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(713, 435);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem tsmMaintenance;
        private System.Windows.Forms.ToolStripMenuItem tsmMovies;
        private System.Windows.Forms.ToolStripMenuItem tsmUsers;
        private System.Windows.Forms.ToolStripMenuItem tsmRent;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem titleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem directorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfUsersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listOfMoivesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allAvailableMoviesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allRentedMoviesToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}

