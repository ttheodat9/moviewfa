﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;

namespace MainForm
{
    public partial class Users : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_movies;Integrated Security=True");
        int userID;
        public Users()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnInsert_Click(object sender, EventArgs e)
        {
            if (txtName.Text != "" && txtAddress.Text != "" && txtPhone.Text != "" && txtEmail.Text != "" && txtZipCode.Text != "")
            {
                DialogResult question = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        int zipCode = int.Parse(txtZipCode.Text);

                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO users VALUES ('{txtName.Text}', '{txtAddress.Text}', '{txtPhone.Text}', '{txtEmail.Text}', {zipCode})");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        btnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Record successfully inserted!");
                        txtName.Text = "";
                        txtAddress.Text = "";
                        txtPhone.Text = "";
                        txtEmail.Text = "";
                        txtZipCode.Text = "";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        connection.Close();
                    }
                }
            }
        }
        private int GetUserID()
        {
            int userID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT user_id FROM users WHERE user_name = '{txtName.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                userID = Convert.ToInt16(table.Rows[0]["user_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return userID;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            userID = GetUserID();
            DialogResult question = MessageBox.Show($"Are you sure you want to update the user {txtName.Text} with the ID:{userID}?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (question == DialogResult.Yes)
            {
                try
                {
                    int zipCode = int.Parse(txtZipCode.Text);
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = String.Format($"UPDATE users SET user_name = '{txtName.Text}', user_address = '{txtAddress.Text}', user_phone = '{txtPhone.Text}', user_email = '{txtEmail.Text}', user_zip_code = {zipCode}  WHERE user_id = {userID}");
                    comm.ExecuteNonQuery();
                    connection.Close();
                    btnInsert.Text = "Insert";
                    btnUpdate.Enabled = true;
                    connection.Close();
                    MessageBox.Show("Record successfully updated!");
                    txtName.Text = "";
                    txtAddress.Text = "";
                    txtPhone.Text = "";
                    txtEmail.Text = "";
                    txtZipCode.Text = "";

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }
        }

    }
}
